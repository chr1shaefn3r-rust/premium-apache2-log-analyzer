use crate::count::IconAndDescription;
use crate::CountMap;
use crate::CountName;

pub fn print_stat(counts: &CountMap, name: &CountName) {
    output_stat(counts.get(name), name.display());
}

pub fn output_stat(count: Option<&i32>, description: IconAndDescription) {
    print(description, count);
}

fn print(description: IconAndDescription, count: Option<&i32>) {
    let (icon, text) = description;
    match count {
        Some(count) => println!("{} {:5} {}", icon, count, text),
        None => (),
    }
}

pub struct Options {
    pub verbose: bool,
    pub paths: Vec<String>,
}

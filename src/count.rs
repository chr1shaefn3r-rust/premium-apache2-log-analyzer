use std::collections::HashMap;

pub type CountMap = HashMap<CountName, i32>;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum CountName {
  Days,
  SuccessfulRequest,
  AppStartRequest,
  SearchRequest,
  FeedbackRequest,
  NonStandardItemList,
  NonDefaultCountriesItemList,
  NonDefaultTypesItemList,
  NonDefaultProductsItemList,
}

pub type IconAndDescription = (&'static str, &'static str);

impl CountName {
  pub const fn display(&self) -> IconAndDescription {
    match *self {
      CountName::Days => ("📆", "Days"),
      CountName::SuccessfulRequest => ("✅", "All successfull requests"),
      CountName::AppStartRequest => ("👤", "Users"),
      CountName::SearchRequest => ("🔍", "Searches"),
      CountName::FeedbackRequest => ("📣", "Feedbacks"),
      CountName::NonStandardItemList => ("📋", "Non-standard item list"),
      CountName::NonDefaultCountriesItemList => ("📋", "Non-default countries item list"),
      CountName::NonDefaultTypesItemList => ("📋", "Non-default types item list"),
      CountName::NonDefaultProductsItemList => ("📋", "Non-default products item list"),
    }
  }
}

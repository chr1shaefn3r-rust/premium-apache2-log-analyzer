use crate::CountMap;
use crate::CountName;
use const_format::concatcp;

const SUCCESSFUL_REQUEST: &str = "HTTP/1.1\" 200";
const ITEM_LIST_REQUEST: &str = "GET /item/list";
const SEARCH_REQUEST: &str = "GET /search/";
const FEEDBACK_REQUEST: &str = "POST /feedback";
const DEFAULT_COUNTRIES: &str = "countries[]=DE&countries[]=AT&countries[]=CH";
const DEFAULT_TYPES: &str = "types[]=laeden&types[]=haendler&types[]=sprecher&types[]=webshop";
const DEFAULT_PRODUCTS: &str =
    "products[]=cola&products[]=bier&products[]=frohlunder&products[]=muntermate";
const APP_START_REQUEST: &str = concatcp!(
    ITEM_LIST_REQUEST,
    "?",
    DEFAULT_TYPES,
    "&",
    DEFAULT_COUNTRIES,
    "&",
    DEFAULT_PRODUCTS,
    " ",
    SUCCESSFUL_REQUEST
);

pub fn count_line(counts: &mut CountMap, line: String) {
    if line.contains(SUCCESSFUL_REQUEST) {
        increment(counts, CountName::SuccessfulRequest);
    }
    if line.contains(SEARCH_REQUEST) {
        increment(counts, CountName::SearchRequest);
        return;
    }
    if line.contains(FEEDBACK_REQUEST) {
        increment(counts, CountName::FeedbackRequest);
        return;
    }
    if line.contains(APP_START_REQUEST) {
        increment(counts, CountName::AppStartRequest);
    }
    if line.contains(ITEM_LIST_REQUEST) && !line.contains(APP_START_REQUEST) {
        increment(counts, CountName::NonStandardItemList);
    }
    if line.contains(ITEM_LIST_REQUEST) && !line.contains(DEFAULT_COUNTRIES) {
        increment(counts, CountName::NonDefaultCountriesItemList);
    }
    if line.contains(ITEM_LIST_REQUEST) && !line.contains(DEFAULT_TYPES) {
        increment(counts, CountName::NonDefaultTypesItemList);
    }
    if line.contains(ITEM_LIST_REQUEST) && !line.contains(DEFAULT_PRODUCTS) {
        increment(counts, CountName::NonDefaultProductsItemList);
    }
}

pub fn increment(counts: &mut CountMap, name: CountName) {
    let count = counts.entry(name).or_insert(0);
    *count += 1;
}

pub fn combine_stats(left: &CountMap, right: &CountMap) -> CountMap {
    let mut combined_counts = left.clone();
    for (name, count) in right {
        let left_count = combined_counts.entry(name.to_owned()).or_insert(0);
        *left_count += count;
    }
    combined_counts
}

pub fn calculate_users_per_day(counts: CountMap) -> Option<i32> {
    match counts
        .get(&CountName::Days)
        .zip(counts.get(&CountName::AppStartRequest))
    {
        Some((count_days, count_app_start_request)) => Some(count_app_start_request / count_days),
        None => None,
    }
}

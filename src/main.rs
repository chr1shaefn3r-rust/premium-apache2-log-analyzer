use argparse::{ArgumentParser, Collect, StoreTrue};
use rayon::prelude::*;
use std::io::*;

mod arg;
mod compute;
mod count;
mod file;
mod output;

use arg::Options;
use compute::*;
use count::CountMap;
use count::CountName;
use output::*;

fn main() {
    let mut opts = Options {
        paths: ["test/sample_1".to_string()].to_vec(),
        verbose: false,
    };

    {
        let mut ap = ArgumentParser::new();
        ap.set_description("Parse api.landkarte.premium-cola.de apache2 log files.");
        ap.refer(&mut opts.verbose)
            .add_option(&["-v", "--verbose"], StoreTrue, "Be verbose");
        ap.refer(&mut opts.paths).add_argument(
            "FILE",
            Collect,
            "Paths to the log file(s) to process",
        );
        ap.parse_args_or_exit();
    }

    let counts = opts
        .paths
        .par_iter()
        .map(|path| file_stats(&path, &opts))
        .reduce_with(|a, b| combine_stats(&a, &b))
        .unwrap();

    print_stat(&counts, &CountName::AppStartRequest);
    print_stat(&counts, &CountName::SearchRequest);
    print_stat(&counts, &CountName::FeedbackRequest);
    print_stat(&counts, &CountName::NonStandardItemList);
    print_stat(&counts, &CountName::NonDefaultTypesItemList);
    print_stat(&counts, &CountName::NonDefaultProductsItemList);
    print_stat(&counts, &CountName::NonDefaultCountriesItemList);
    print_stat(&counts, &CountName::Days);
    output_stat(
        calculate_users_per_day(counts).as_ref(),
        ("📊", "Users per Day"),
    );
}

pub fn file_stats(path: &str, opts: &Options) -> CountMap {
    let file_stream = file::reader(&path, &opts);
    stream_stats(file_stream, &opts)
}

pub fn stream_stats(stream: Box<dyn BufRead>, opts: &Options) -> CountMap {
    let mut times = CountMap::default();
    increment(&mut times, CountName::Days);
    let mut lineno = 0;

    stream.lines().for_each(|line| {
        if opts.verbose {
            lineno += 1;
            if lineno % 100 == 0 {
                print!(".");
                stdout().flush().unwrap();
            }
        }

        match line {
            Ok(l) => {
                count_line(&mut times, l);
            }
            Err(e) => {
                if opts.verbose {
                    eprintln!("Failed to read line:\n  {}", e);
                }
            }
        }
    });

    if opts.verbose {
        println!("");
    }

    times
}

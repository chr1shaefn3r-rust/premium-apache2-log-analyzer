# premium-apache2-log-analyzer

Designed to calculate basic feature usage data from apache2 log files, thanks to rust pretty quickly.

## Usage

run `bash analyze.sh` and make sure you have "premium" configured as ssh alias

## Speed comparison

all numbers taken with hyperfine on a AMD Ryzen 7 1700X Eight-Core Processor running Ubuntu 20.04, analyzing a set of 10 (~60kb gziped) and 40 days (~216kb gzipped) worth of logs

### ~v1.1.0

```bash
$ bash bench.sh 
    Finished release [optimized] target(s) in 0.02s

commit 8a80b652b3bdd61248138404b09cbeb1ac15add5
Benchmark #1: target/release/premium-apache2-log-analyzer ./40/*
  Time (mean ± σ):       6.6 ms ±   1.1 ms    [User: 30.6 ms, System: 11.2 ms]
  Range (min … max):     5.1 ms …  14.1 ms    382 runs

$ bash bench.sh 
    Finished release [optimized] target(s) in 0.02s

commit 8a80b652b3bdd61248138404b09cbeb1ac15add5
Benchmark #1: target/release/premium-apache2-log-analyzer ./10/*
  Time (mean ± σ):       4.2 ms ±   0.4 ms    [User: 8.8 ms, System: 8.3 ms]
  Range (min … max):     3.2 ms …   6.1 ms    575 runs
 
  Warning: Command took less than 5 ms to complete. Results might be inaccurate.
 
```

### ~v1.0.1

```bash
$ bash bench.sh 
    Finished release [optimized] target(s) in 0.01s

commit ebb5db0d16fe9ed4ca9b2edaea855ec1c5b34965
Benchmark #1: target/release/premium-apache2-log-analyzer ./40/*
  Time (mean ± σ):      24.4 ms ±   1.7 ms    [User: 23.2 ms, System: 1.0 ms]
  Range (min … max):    19.6 ms …  27.5 ms    125 runs

$ bash bench.sh 
    Finished release [optimized] target(s) in 0.01s

commit ebb5db0d16fe9ed4ca9b2edaea855ec1c5b34965
Benchmark #1: target/release/premium-apache2-log-analyzer ./10/*
  Time (mean ± σ):       8.6 ms ±   1.2 ms    [User: 7.7 ms, System: 0.8 ms]
  Range (min … max):     5.5 ms …  10.5 ms    297 runs
 
```

### old 

The first version was written in bash (previously/apache2-log-analysis.sh) and with a far superior algorithm so the comparison to this rust version is by no means fair:

```bash
$ hyperfine --warmup 3 'bash apache2-log-analysis.sh ./40/*'
Benchmark #1: bash apache2-log-analysis.sh ./40/*
  Time (mean ± σ):      2.847 s ±  0.083 s    [User: 1.252 s, System: 0.255 s]
  Range (min … max):    2.735 s …  2.996 s    10 runs
$ hyperfine --warmup 3 'bash apache2-log-analysis.sh ./10/*'
Benchmark #1: bash apache2-log-analysis.sh ./10/*
  Time (mean ± σ):      2.781 s ±  0.067 s    [User: 1.255 s, System: 0.253 s]
  Range (min … max):    2.676 s …  2.854 s    10 runs
 
```

## Credits

This repo is heavily inspired and in many parts copied from [https://github.com/indirect](@indirect)'s [https://github.com/rubytogether/kirby/](kirby).
Recommended read: [https://andre.arko.net/2018/10/25/parsing-logs-230x-faster-with-rust/](Parsing logs 230x faster with Rust)

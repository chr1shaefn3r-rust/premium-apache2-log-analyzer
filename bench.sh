#!/bin/bash

command -v hyperfine > /dev/null || (echo "You need to install 'hyperfine' with cargo or brew." && exit 1)

cargo build --release

echo
echo "commit $(git log -1 --pretty=format:%H)"
hyperfine --warmup 3 'target/release/premium-apache2-log-analyzer ./test/*'

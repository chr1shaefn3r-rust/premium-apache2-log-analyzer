#!/bin/bash

set -e # Exit immediately if a simple command exits with a non-zero status

cargo build --release

mkdir -p log-analysis

# Download access logs
echo "Downloading access logs"
rsync -e ssh premium:/var/log/apache2/api.landkarte.premium-cola.de.access.log.*.gz ./log-analysis/

echo "Start analysis"
./target/release/premium-apache2-log-analyzer ./log-analysis/*
